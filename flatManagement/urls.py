from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import views as auth_views
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import verify_jwt_token
from rest_framework.urlpatterns import format_suffix_patterns

from userPanel import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    path('', auth_views.login, name='login'),
    path('getToken/', obtain_jwt_token),
    path('checkToken/',verify_jwt_token),
    path('logout/', auth_views.logout, name='logout'),
    path('userPanel/', include('userPanel.urls')),
    path('api/', include('userPanel.api.urls')),
    path('admin/', admin.site.urls),

]

if settings.DEBUG == False:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


urlpatterns = format_suffix_patterns(urlpatterns)

