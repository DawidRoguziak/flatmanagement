from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path

from . import views

urlpatterns = {
    # JSON
    # login
    # grupy
    path('groupsOfItems/', views.groupList.as_view(), name='groupList'),
    # path('oneGroupsOfItems/<int:pk>/', views.singleOfGroup.as_view(), name='singleGroup'),
    # path('updateGroup/<int:pk>', views.updateGroup.as_view(), name='updateGroup'),
    path('deleteGroup/<int:pk>', views.deleteGroup.as_view(), name='deleteGroup'),
    # # itemy
    path('itemList/', views.itemList.as_view(), name='itemList'),
    path('postList/', views.postList.as_view(), name='postList'),
    path('userList/', views.userListView.as_view(), name='usersList'),
    # path('updateItem/<int:group>/<int:id>', views.updateItem.as_view(), name='updateItem'),
}
