from rest_framework.generics import ListAPIView, UpdateAPIView, RetrieveAPIView, DestroyAPIView
from django.shortcuts import get_object_or_404
from userPanel.models import commonItems, groupsOfItems,Post
from userPanel.serializer import itemSeralizer, groupOfItemsSeralizer, userSerializer , postSerializer
from django.db.models import Q
from django.contrib.auth.models import User

from rest_framework import mixins

#test
class userListView(ListAPIView):
    serializer_class = userSerializer

    def get_queryset(self, *args, **kwargs):
        queryset = User.objects.all()
        username = self.request.query_params.get('username', None)
        if username is not None:
            queryset = queryset.filter(username=username)

        return queryset


# itemy
class itemList(ListAPIView):
    serializer_class = itemSeralizer

    def get_queryset(self, *args, **kwargs):
        queryset = commonItems.objects.all()
        group = self.request.query_params.get('id', None)

        if group is not None:
            queryset = queryset.filter(group=group)

        return queryset


# class updateItem(UpdateAPIView):
#     queryset = commonItems.objects.all()
#     serializer_class = itemSeralizer
#     lookup_field = 'id'
#

# grupy
class groupList(ListAPIView):
    serializer_class = groupOfItemsSeralizer

    def get_queryset(self, *args, **kwargs):
        queryset = groupsOfItems.objects.all()
        user = self.request.query_params.get('id', None)
        if user is not None:
            queryset = queryset.filter(user=user)

        return queryset


# class updateGroup(UpdateAPIView):
#    # queryset = groupsOfItems.objects.all()
#     serializer_class = groupOfItemsSeralizer

class postList(ListAPIView):
    serializer_class = postSerializer

    def get_queryset(self, *args, **kwargs):
        queryset = Post.objects.all()
        user = self.request.query_params.get('id', None)
        if user is not None:
            queryset = queryset.filter(user=user)

        return queryset

#
class deleteGroup(DestroyAPIView):
    queryset = groupsOfItems.objects.all()
    serializer_class = groupOfItemsSeralizer
# curl -H "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJ1c2VybmFtZSI6InJvb20xIiwiZXhwIjoxNTI4NTc1Mjk0LCJlbWFpbCI6IiJ9.4YhUt4WUO-g4Dx0ftqRy_bqPOnVeevWCsgPsIildCDM" -d username="dr" http://212.182.24.41:8000/api/groupsOfItems/
