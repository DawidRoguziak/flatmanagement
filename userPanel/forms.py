from django import forms

from .models import Post, groupsOfItems, commonItems, schedule, flatMateQueue, flatMates


class updateFlatMatesWeb(forms.ModelForm):
    class Meta:
        model = flatMates
        fields = [
            "name",
            "surrname",
        ]


class updateScheduleWeb(forms.ModelForm):
    class Meta:
        model = schedule
        fields = [
            "name",
            "queue",
            "weeks",
        ]


class updateflatMateQWeb(forms.ModelForm):
    class Meta:
        model = flatMateQueue
        fields = [
            "name",
            "userQ",
        ]


class updatePostWeb(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            "title",
            "content"
        ]


class updateGroupWeb(forms.ModelForm):
    class Meta:
        model = groupsOfItems
        fields = [
            "nameGroup"
        ]


class updateItemWeb(forms.ModelForm):
    class Meta:
        model = commonItems
        fields = [
            "nameItem",
            "amount"
        ]
