from rest_framework.serializers import ModelSerializer, CharField, ValidationError, HyperlinkedIdentityField, \
    SerializerMethodField
from userPanel.models import groupsOfItems, commonItems,Post
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q

from django.contrib.auth import get_user_model

User = get_user_model()


class userSerializer(ModelSerializer):
        class Meta:
            model = User
            fields = ['id']


class groupOfItemsSeralizer(ModelSerializer):
    class Meta:
        model = groupsOfItems
        fields = ['id', 'nameGroup']


class itemSeralizer(ModelSerializer):
    class Meta:
        model = commonItems
        fields = ['group', 'id', 'amount', 'nameItem']

class postSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = ['title','content']

        ordering = ["-updated"]

# class userLoginSerializer(ModelSerializer):
#     username = CharField()
#     token = CharField(allow_blank=True, read_only=True)
#
#     class Meta:
#         model = User
#         fields = [
#             'username',
#             'password',
#             'token',
#         ]
#
#         extra_kwargs = {"password":
#                             {"write_only": True}
#                         }
#
#         def validate(self, data):
#             username = data.get("username", None)
#             password = data["password"]
#             if not username:
#                 raise ValidationError("Nazwa uzytkownika wymagana")
#
#             user = User.objects.filter(
#                 Q(username=username)
#             ).distinct()
#
#             if user.exists() and user.count() == 1:
#                 user_obj = user.first()
#             else:
#                 raise ValidationError("Nazwa uzytkownika bledna")
#
#             if user_obj:
#                 if not user_obj.check_password(password):
#                     raise ValidationError("Zle chasło ")
#
#             data["token"] = "SOME RANDOM TOKEN"
#
#             return data
