from django.contrib import admin
from .models import commonItems
from .models import groupsOfItems
from .models import Post
from .models import flatMates

from .models import flatMateQueue,schedule

admin.site.register(commonItems)
admin.site.register(groupsOfItems)
admin.site.register(Post)
admin.site.register(flatMates)
admin.site.register(flatMateQueue)
admin.site.register(schedule)
