from django.urls import include, path

from . import views

urlpatterns = [
    # pomocnicze
    path('def/', views.leftSideDef, name="def"),
    path('', views.welPage, name="main"),
    # posty
    path('putPost/', views.putPost, name='putPost'),
    path('postCreate/', views.postCreate, name="postCreate"),
    path('post/', views.putPost, name="post"),
    path('postDelete/<int:id>', views.postDelete, name="postDelete"),
    # grupy i itemy
    path('itemGroups/', views.groupOfItmesView, name="groups"),
    path('groups/<int:id>/', views.detailOfGroup, name="detail"),
    path('addGroup/', views.addGroup, name="groupCreate"),
    path('itemGroups/groupDelete/<int:id>', views.groupDelete, name="groupDelete"),
    path('addItem/<int:id>', views.addItem, name="addItem"),
    path('deleteItem/<int:idG>/<int:idI>/', views.deleteItem, name="deleteItem"),
    path('plusItem/<int:idG>/<int:idI>/', views.addOneUnit, name="plusItem"),
    path('minusItem/<int:idG>/<int:idI>/', views.minusOneUnit, name="minusItem"),
    # grafiki
    path('schedule/', views.putSchedule, name="schedule"),
    path('addFlatMateQ/', views.faltMateListCreate, name="cerateFlatMateQ"),
    path('addSchedule/', views.scheduleCreate, name="cerateSchedule"),
    path('scheduleDelete/', views.scheduleDelete, name="scheduleDelete"),
    path('addFlatMates/', views.addFlateMates, name="addMate"),

]
