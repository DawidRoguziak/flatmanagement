from django.db import models
from django.utils import timezone
from datetime import datetime
from django.urls import reverse
import json
from django.contrib.auth.models import User


class groupsOfItems(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    nameGroup = models.CharField(max_length=100)

    def __str__(self):
        return " " + self.nameGroup + " "


class commonItems(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    nameItem = models.CharField(max_length=100)
    amount = models.IntegerField(null=True)

    group = models.ForeignKey(groupsOfItems, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.nameItem)


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    title = models.CharField(max_length=100)

    updated = models.DateTimeField(auto_now_add=True, blank=True)

    # time = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-updated"]


def __str__(self):
    return str(self.title)


# def get_absolute_url(self):
#     return reverse("posts:detail", kwargs={"id": self.id})


class flatMateQueue(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=25)
    userQ = models.CharField(max_length=255)

    def __str__(self):
        return str(self.name)

    def listUsers(self):
        return self.userQ.split(',')

    def numberOfFlatMates(self):
        return self.userQ.split(',').__len__()


class schedule(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    queue = models.ForeignKey(flatMateQueue, on_delete=models.CASCADE, null=True)
    weeks = models.CharField(max_length=300)
    current = models.IntegerField(default=0)
    week = models.IntegerField(default=0)
    currentWeek = models.CharField(max_length=20)

    def __str__(self):
        return str(self.name)

    def listWeeks(self):
        return self.weeks.split(',')

    def listU(self):
        return self.queue.listUsers()

    def getCurrentWeek(self):
        return self.currentWeek

    def incCurrent(self):
        maxF = self.queue.numberOfFlatMates()
        listW = self.listWeeks()

        self.current += 1
        if self.current < maxF:
            self.currentWeek = listW[self.current]


class flatMates(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    surrname = models.CharField(max_length=50)

    def __str__(self):
        return str(self.name + self.surrname)
