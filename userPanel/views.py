from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect, render_to_response

from .models import groupsOfItems, groupsOfItems
from .models import Post
from .models import flatMates, flatMateQueue

from .models import schedule

from .forms import updatePostWeb, updateFlatMatesWeb, updateGroupWeb, updateItemWeb, updateflatMateQWeb, \
    updateScheduleWeb
from django.contrib.auth.decorators import login_required

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, DestroyAPIView
from userPanel.serializer import groupOfItemsSeralizer, itemSeralizer, groupOfItemsSeralizer
# from .models import fiveWeeks
from userPanel.models import commonItems, groupsOfItems

import datetime


@login_required
def scheduleCreate(request):
    weeks = ""
    date = datetime.date.today()
    cW = ""

    startWeek = date - datetime.timedelta(date.weekday())
    endWeek = startWeek + datetime.timedelta(6)

    queueList = flatMateQueue.objects.filter(user=request.user)
    quLen = flatMateQueue.objects.first()
    q = flatMateQueue.numberOfFlatMates(quLen)

    t1 = startWeek.strftime("%d-%m")
    t2 = endWeek.strftime("%d-%m")
    weeks += t1
    weeks += " - "
    weeks += t2
    cW = weeks
    weeks += ","

    date = endWeek

    for i in range(0, q-1):
        startWeek = date - datetime.timedelta(0)
        endWeek = startWeek + datetime.timedelta(7)
        date = endWeek
        endWeek = endWeek - datetime.timedelta(0)
        startWeek = startWeek + datetime.timedelta(1)
        t1 = startWeek.strftime("%d-%m")
        t2 = endWeek.strftime("%d-%m")
        weeks += t1
        weeks += " - "
        weeks += t2
        if i != q-2:
            weeks += ","

    form = updateScheduleWeb(request.POST or None)

    if request.method == 'POST':

        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.currentWeek = cW
            instance.save()

    context = {
        "form": form,
        "weeks": weeks,
        "queueList": queueList,
        # "duration": duration,

    }

    return render(request, "userPanel/addschedule.html", context)


@login_required
def putSchedule(request):
    list = schedule.objects.filter(user=request.user)
    date = datetime.date.today()
    # startWeek = date - datetime.timedelta(date.weekday())
    # endWeek = startWeek + datetime.timedelta(6)

    today = date.strftime("%d")
    todayM = date.strftime("%m")
    for i in list:
        qL = i.queue.numberOfFlatMates()
        qCur = i.current
        curWeek = i.currentWeek
        curWeek = curWeek.split(' - ')
        end = curWeek[1].split('-')
        start = curWeek[0].split('-')
        endD = end[0]
        endM = end[1]
        startM = start[1]
        if qCur != qL:
            if endM == todayM:
                if endD < today:
                    i.incCurrent()
                    i.save()
        else:
            i.delete()

    #list = schedule.objects.filter(user=request.user)

    date = datetime.date.today()
    today = date.strftime("%d-%m")
    context = {
        "list": list,
        "day": today,
    }
    return render(request, "userPanel/schedule.html", context)


@login_required
def scheduleDelete(request, id):
    instance = get_object_or_404(schedule, id=id)
    instance.delete()
    return HttpResponse("ok")


@login_required
def faltMateListCreate(request):
    form = updateflatMateQWeb(request.POST or None)
    userList = flatMates.objects.filter(user=request.user)
    count = userList.count()
    if request.method == 'POST':

        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()

    context = {
        "userList": userList,
        "form": form,
        "count": count,
    }

    return render(request, "userPanel/addListOfMates.html", context)


@login_required
def addFlateMates(request):
    form = updateFlatMatesWeb(request.POST or None)
    if request.method == 'POST':

        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()

    context = {
        "form": form,
    }

    return render(request, "userPanel/addFlatMates.html", context)


@login_required
def flatMateQDelete(request, id):
    instance = get_object_or_404(flatMateQueue, id=id)
    instance.delete()
    return HttpResponse("ok")


# posty
@login_required
def postCreate(request):
    form = updatePostWeb(request.POST or None)
    if request.method == 'POST':

        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()

    context = {
        "form": form,
    }

    return render(request, "userPanel/addPost.html", context)


@login_required
def putPost(request):
    queryset = Post.objects.filter(user=request.user)
    # paginator = Paginator(queryset_list, 25)  # Show 25 contacts per page
    #
    # page = request.GET.get('page')
    # queryset = paginator.get_page(page)
    #

    context = {'obj': queryset}

    return render(request, 'userPanel/index.html', context)


@login_required
def postDelete(request, id):
    instance = get_object_or_404(Post, pk=id)
    instance.delete()

    return redirect('/userPanel/putPost/')


# grupy
@login_required
def groupOfItmesView(request):
    allGroups = groupsOfItems.objects.filter(user=request.user)

    groupsLen = allGroups.__len__()
    groupsLen = groupsLen / 3
    context = {'allGroups': allGroups, 'len': groupsLen}

    return render(request, 'userPanel/groupListView.html', context)


@login_required
def detailOfGroup(request, id):
    group = get_object_or_404(groupsOfItems, pk=id)

    return render(request, 'userPanel/groupDetail.html', {'groupDetail': group, 'id': id})



@login_required
def addGroup(request):
    form = updateGroupWeb(request.POST or None)
    if request.method == 'POST':

        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()

    context = {
        "form": form,
    }

    return render(request, "userPanel/addGroup.html", context)

@login_required
def groupDelete(request, id):
    instance = get_object_or_404(groupsOfItems, id=id)
    instance.delete()
    return redirect('/userPanel/itemGroups/')


#items
@login_required
def addItem(request, id):
    form = updateItemWeb(request.POST or None)
    if request.method == 'POST':

        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.group = get_object_or_404(groupsOfItems, pk=id)
            instance.save()

    context = {
        "form": form,
        "id": id
    }

    return render(request, "userPanel/addItem.html", context)

@login_required
def deleteItem(request, idI, idG):
    instance = get_object_or_404(commonItems, pk=idI)
    instance.delete()
    return redirect('/userPanel/groups/'+str(idG))

@login_required
def addOneUnit(requset,idI,idG):
    instance = get_object_or_404(commonItems,id=idI)
    instance.amount = instance.amount + 1
    instance.save()
    return redirect('/userPanel/groups/'+str(idG))

def minusOneUnit(requset,idI,idG):
    instance = get_object_or_404(commonItems,id=idI)
    if 0 < instance.amount:
        instance.amount = instance.amount - 1
    instance.save()
    return redirect('/userPanel/groups/' + str(idG))

# pomocnicza strona
@login_required
def leftSideDef(request):
    return render(request, 'userPanel/leftSideDef.html')
@login_required
def welPage(request):
    return render(request, 'userPanel/welcomPage.html')

# ----------------------------------V---------------------------------------------
# ---------------------------------VVV---------------------------------------------
# -------------------------------VVVVVVV--------------------------------------------
# ------------------------------VVVVVVVVVV------------------------------------------
